; Ensures only a single instance of this script exists.
#SingleInstance force

; Gets Final Fantasy XIV from the list of processes.
WinGet, pid, List, FINAL FANTASY

; Leave these alone. Should be decent values.
shortDelay = 350
modifierDelay = 150
one = 500

/*
	/////////////////////////////////////////////////////////////////////////////////////
*/


/*
	/////////////////////////////////////////////////////////////////////////////////////
*/

; Keys - looks these keystrokes up at https://autohotkey.com/docs/commands/Send.htm
confirmButton = V
navigateRight = Right

; Speed Mode (default = false)
speedMode = true

/*
	/////////////////////////////////////////////////////////////////////////////////////
*/

; Please change timing only if it's unstable for you.
placeWait = 1550
buff = 2250
synth = 3150
complete = 4650
completeHQ = 5650
completeCC = 4650
menuPopUp = 400
speedLoop = 1
speedSleep = 0

if (%speedMode%) {
	placeWait := 1000
	buff := 750
	synth := 1750
	complete := 4650
	completeHQ := 5650
	completeCC := 4650
	menuPopUp := 400
	speedLoop := 5
	speedSleep := 100
	one := 300
}

; Shortcuts for the script itself.
PgDn::Pause
^PgDn::Reload
^!PgDn::ExitApp

/*
	/////////////////////////////////////////////////////////////////////////////////////
*/

/*
	/////////////////////////////////////////////////////////////////////////////////////
	UI Functions
	/////////////////////////////////////////////////////////////////////////////////////
*/

Wait() {
	global
	Sleep %one%
	Return
}

PlaceItemWait() {
	global
	Sleep %placeWait%
	Return
}

ConfirmItemSelection(num) {
	global
	Loop %num% {
		ControlSend,, {%confirmButton%}, ahk_id %pid1%
		Sleep %one%
	}
	Return
}

; Refocuses the in-game cursor to the crafting window after every synthesis.
RefocusWindow() {
	global
	Loop 1 {
		ControlSend,, {%navigateRight%}, ahk_id %pid1%
		Sleep %shortDelay%
	}
	Return
}

; Swaps to the desired hotbar.
HotBarSwap(input) {
	global
	ControlSend,, {Shift Down}, ahk_id %pid1%
	Sleep %modifierDelay%

	Loop %speedLoop% {
		ControlSend,, {%input%}, ahk_id %pid1%
		if (%speedMode%) {
			Sleep %speedSleep%
		}
	}

	Sleep %modifierDelay%
	ControlSend,, {Shift Up}, ahk_id %pid1%
	Sleep %shortDelay%
	Return
}


/*
	/////////////////////////////////////////////////////////////////////////////////////
	Synthesis Functions
	/////////////////////////////////////////////////////////////////////////////////////
*/

Synth(input, repeat:=1) {
	global
	Loop %repeat% {
		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}
		Sleep %synth%
	}
	Return
}

AltSynth(input, repeat:=1) {
	global
	Loop %repeat% {
		ControlSend,, {Alt Down}, ahk_id %pid1%
		Sleep %modifierDelay%

		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}

		Sleep %modifierDelay%
		ControlSend,, {Alt Up}, ahk_id %pid1%
		Sleep %synth%
	}
	Return
}

CtrlSynth(input, repeat:=1) {
	global
	Loop %repeat% {
		ControlSend,, {Ctrl Down}, ahk_id %pid1%
		Sleep %modifierDelay%

		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}

		Sleep %modifierDelay%
		ControlSend,, {Ctrl Up}, ahk_id %pid1%
		Sleep %synth%
	}
	Return
}

/*
	/////////////////////////////////////////////////////////////////////////////////////
	Finish Synthesis Functions
	Call one of these functions when you are on the last step.
	NQ, HQ, and Collectability all have differing animation delays.
	/////////////////////////////////////////////////////////////////////////////////////
*/
CompleteSynthNQ(input) {
	global

	Loop %speedLoop% {
		ControlSend,, {%input%}, ahk_id %pid1%
		if (%speedMode%) {
			Sleep %speedSleep%
		}
	}

	Sleep %complete%
	Return
}

CompleteSynthHQ(input) {
	global

	Loop %speedLoop% {
		ControlSend,, {%input%}, ahk_id %pid1%
		if (%speedMode%) {
			Sleep %speedSleep%
		}
	}

	Sleep %completeHQ%
	Return
}

CompleteSynthCC(input) {
	global

	Loop %speedLoop% {
		ControlSend,, {%input%}, ahk_id %pid1%
		if (%speedMode%) {
			Sleep %speedSleep%
		}
	}

	Sleep %synth%
	Sleep %menuPopUp%
	ControlSend,, {%confirmButton%}, ahk_id %pid1%
	Sleep %one%
	ControlSend,, {%confirmButton%}, ahk_id %pid1%
	Sleep %one%
	Sleep %completeCC%
	Return
}


/*
	/////////////////////////////////////////////////////////////////////////////////////
	Buff Functions
	/////////////////////////////////////////////////////////////////////////////////////
*/

Buff(input, repeat:=1) {
	global
	Loop %repeat% {
		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}
		Sleep %buff%
	}
	Return
}

CtrlBuff(input, repeat:=1) {
	global
	Loop %repeat% {
		ControlSend,, {Ctrl Down}, ahk_id %pid1%
		Sleep %modifierDelay%

		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}

		Sleep %modifierDelay%
		ControlSend,, {Ctrl Up}, ahk_id %pid1%
		Sleep %buff%
	}
	Return
}

AltBuff(input, repeat:=1) {
	global
	Loop %repeat% {
		ControlSend,, {Alt Down}, ahk_id %pid1%
		Sleep %modifierDelay%

		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}

		Sleep %modifierDelay%
		ControlSend,, {Alt Up}, ahk_id %pid1%
		Sleep %buff%
	}
	Return
}

CustomBuff(input, time, repeat:=1) {
	global
	Loop %repeat% {
		Loop %speedLoop% {
			ControlSend,, {%input%}, ahk_id %pid1%
			if (%speedMode%) {
				Sleep %speedSleep%
			}
		}

		Sleep %time%
	}
	Return
}
