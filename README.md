# Deprecated
I'm working on a new library to replace this one entirely. It can be found here at https://gitlab.com/nullbahamut/ffxiv-crafting-library.

# FFXIV AutoHotKey Library

A library (in progress) that I've made that simplifies automating crafting up to a certain degree.


# Requirements

- AutoHotKey


# Updates

This library is still in progress but slowly being updated. One of the major things 
to come is breaking up the library into multiple files (similar to implementations of classes).
This way, navigating through the API is a bit more tolerable.


# Functionality

Each function is implemented to reduce the amount of specific and repetitious scripting
for crafting.

For example, invoking `Synth(hotkey, repeat)` will handle proper waits and looping
to reduce excessive script bulk and improve readability.

All the sleep values are modifiable to your liking and each function is easily 
defineable so othat you will be able to script to your liking (without the mess of
hotkey binding).