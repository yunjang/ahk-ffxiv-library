#SingleInstance force
#Include %A_ScriptDir%\..\Library\CraftingLibrary.ahk

^b::
	InputBox, quantity, Enter Quantity, Enter how many you want to craft.

	Wait()

	Loop %quantity%
	{
		RefocusWindow()
		ConfirmItemSelection("4")
		PlaceItemWait()

		HotBarSwap("2")
		CustomBuff("1", "3500")
		Buff("0")
		Buff("-")
		Synth("=", "2")
		Synth("3", "3")
		Buff("-")
		Synth("3", "5")

		HotBarSwap("1")
		Buff("8")
		Buff("9")
		Buff("0")
		Synth("1", "2")
		Buff("-")
		Synth("2", "5")
		Buff("8")
		Synth("2", "1")
		AltSynth("2")

		Buff("-")
		Synth("2", "5")
		Buff("4")
		CtrlBuff("2")
		Buff("5")
		Synth("6", "1")
		CompleteSynthHQ("1")
	}
Return
