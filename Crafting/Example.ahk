; These two should always be included when creating a new script.
#SingleInstance force
#Include %A_ScriptDir%\..\Library\CraftingLibrary.ahk

^b::
	InputBox, quantity, Enter Quantity, Enter how many you want to craft.

	Wait()

	Loop %quantity%
	{
		RefocusWindow()           ; Taps the in-game cursor movement to the right to refocus.
		ConfirmItemSelection("4") ; Taps the confirm button 4 times (to ensure it gets to the Synthesize button)
		PlaceItemWait()           ; Accounts for latency and animation time when crafting your item.

		HotBarSwap("1")           ; Swaps hotbar to number 1 (or whichever you choose)
    Synth("1")                ; Presses hotkey #1 just once.
    Synth("1", "3")           ; Presses hotkey #1 three times.
		CompleteSynthNQ("1")      ; Presses hotkey #1 and completes the synthesis by waiting for the NQ animation delay.
	}
Return
