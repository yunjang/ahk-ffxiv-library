#SingleInstance force
#Include %A_ScriptDir%\..\Library\CraftingLibrary.ahk

^b::
	InputBox, quantity, Enter Quantity, Enter how many you want to craft.

	Wait()

	Loop %quantity%
	{
		RefocusWindow()
		ConfirmItemSelection("4")
		PlaceItemWait()

		Buff("8")
		Buff("9")
		Buff("-")
		Buff("=")
		Synth("3", "4")
		Buff("-")
		Synth("3", "2")
		Buff("4")
		CtrlBuff("2")
		Buff("5")
		CtrlBuff("4")
		Synth("6")
		CtrlSynth("=")
		CompleteSynthHQ("1")
	}
Return
